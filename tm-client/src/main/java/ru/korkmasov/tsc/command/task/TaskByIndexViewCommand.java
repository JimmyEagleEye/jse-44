package ru.korkmasov.tsc.command.task;

import ru.korkmasov.tsc.util.TerminalUtil;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class TaskByIndexViewCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-view-by-index";
    }

    @NotNull
    @Override
    public String description() {
        return "View task by index";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER INDEX:");
        //showTask(serviceLocator.getTaskEndpoint().findTaskByIndex(serviceLocator.getSession(), TerminalUtil.nextNumber()));
    }

}
