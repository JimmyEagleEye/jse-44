package ru.korkmasov.tsc.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.korkmasov.tsc.api.IServiceDto;
import ru.korkmasov.tsc.api.service.IConnectionService;
import ru.korkmasov.tsc.dto.AbstractDtoEntity;

public abstract class AbstractDtoService<E extends AbstractDtoEntity> implements IServiceDto<E> {

    @NotNull
    protected final IConnectionService connectionService;

    @NotNull
    public AbstractDtoService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

}
