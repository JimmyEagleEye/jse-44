package ru.korkmasov.tsc.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.korkmasov.tsc.api.IService;
import ru.korkmasov.tsc.enumerated.Role;
import ru.korkmasov.tsc.model.Session;
import ru.korkmasov.tsc.model.User;

import java.util.List;

public interface ISessionService extends IService<Session> {

    Session open(@Nullable String login, @Nullable String password);

    User checkDataAccess(@Nullable String login, @Nullable String password);

    void validate(@NotNull Session session, Role role);

    void validate(@Nullable Session session);

    Session sign(@Nullable Session session);

    void close(@Nullable Session session);

    void closeAllByUserId(@Nullable String userId);

    @Nullable List<Session> findAllByUserId(@Nullable String userId);
}
