package ru.korkmasov.tsc.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.korkmasov.tsc.api.service.ServiceLocator;
import ru.korkmasov.tsc.api.service.dto.ISessionDtoService;
import ru.korkmasov.tsc.api.service.dto.IUserDtoService;
import ru.korkmasov.tsc.api.service.model.IUserService;
import ru.korkmasov.tsc.dto.SessionDto;
import ru.korkmasov.tsc.dto.UserDto;
import ru.korkmasov.tsc.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@NoArgsConstructor
public final class AdminEndpoint extends AbstractEndpoint {

    private IUserDtoService userDtoService;

    private IUserService userService;

    private ISessionDtoService sessionService;

    public AdminEndpoint(
            @NotNull final ServiceLocator serviceLocator,
            @NotNull final IUserDtoService userDtoService,
            @NotNull final IUserService userService,
            @NotNull final ISessionDtoService sessionService
    ) {
        super(serviceLocator);
        this.userDtoService = userDtoService;
        this.userService = userService;
        this.sessionService = sessionService;
    }

    @WebMethod
    public void removeByLogin(
            @WebParam(name = "session") final SessionDto session, @WebParam(name = "login") final String login
    ) {
        serviceLocator.getSessionDtoService().validate(session, Role.ADMIN);
        userService.removeByLogin(login);
    }

    @WebMethod
    public UserDto lockByLogin(
            @WebParam(name = "session") final SessionDto session, @WebParam(name = "login") final String login
    ) {
        serviceLocator.getSessionDtoService().validate(session, Role.ADMIN);
        return userDtoService.lockByLogin(login);
    }

    @WebMethod
    public UserDto unlockByLogin(
            @WebParam(name = "session") final SessionDto session, @WebParam(name = "login") final String login
    ) {
        serviceLocator.getSessionDtoService().validate(session, Role.ADMIN);
        return userDtoService.unlockByLogin(login);
    }

    @WebMethod
    public void closeAllByUserId(
            @WebParam(name = "session") final SessionDto session,
            @WebParam(name = "userId") final String userId
    ) {
        serviceLocator.getSessionDtoService().validate(session, Role.ADMIN);
        sessionService.closeAllByUserId(userId);
    }

    @Nullable
    @WebMethod
    public List<SessionDto> findAllByUserId(
            @WebParam(name = "session") final SessionDto session,
            @WebParam(name = "userId") final String userId
    ) {
        serviceLocator.getSessionDtoService().validate(session, Role.ADMIN);
        return sessionService.findAllByUserId(userId);
    }
}
